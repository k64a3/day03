<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Đăng ký tân sinh viên</title>
	<style type="text/css">
		* {
			margin: 0;
			padding: 0;
		}

		input {
			padding-left: 5px;
			padding-right: 15px;
		}

		.flex {
			display: flex;
		}

		.relative {
			position: relative;
		}

		.margin-l5 {
			margin-left: 5px;
		}

		.content {
			width: 100%;
			height: 100vh;
			align-items: center
		}

		.container {
			width: 460px;
			height: 300px;
			border: 2px solid #4389fa;
		}

		.main {
			padding: 55px 47px 10px 35px;
		}

		.margin-5 {
			margin: 5px 8px;
		}

		.ele {
			display: flex;
			margin-top: 10px;
		}

		.ele button {
			padding: 8px 0;
			width: 100px;
			background-color: rgb(91, 155, 213);
			border: 2px solid #4389fa;
			color: white;
			margin-right: 10px;
		}

		.ele input {
			outline: none;
			border: 2px solid #4389fa;
		}

		.arrow::after {
			content: "";
			position: absolute;
			right: 2px;
			top: 1px;
			width: 0;
			height: 0;
			border-left: 17px solid transparent;
			border-right: 17px solid transparent;
			border-top: 30px solid rgb(91, 155, 213);
		}

		.arrow::before {
			content: "";
			position: absolute;
			right: 0;
			width: 0;
			height: 0;
			border-left: 19px solid transparent;
			border-right: 19px solid transparent;
			border-top: 33px solid #4389fa;
		}

		.center {
			display: flex;
			justify-content: center;
		}

		.register-container button {
			margin-top: 25px;
			border: 2px solid #4389fa;
			border-radius: 10px;
			background-color: #53c44d;
			padding: 10px 35px;
			color: white;
		}
	</style>
</head>

<body>
	<div class="content center">
		<div class="container">
			<div class="main">
				<div class="ele">
					<button>Họ và tên</button>

					<input type="text" size="30">
				</div>
				<div class="ele">
					<button>Giới tính</button>

					<?php
					$gender = array("Nam", "Nữ");
					for ($i = 0; $i < count($gender); $i++) {
						echo "  <div class='margin-5'>
                                        <input type='radio' name='gender' id='gender-$gender[$i]' value=$i>
                                        <label for='gender-$gender[$i]' class='margin-l5'>$gender[$i]</label>
                                    </div>
                            ";
					}
					?>
				</div>
				<div class="ele">
					<button>Phân khoa</button>

					<div class="flex relative">
						<input list="browsers" name="browser" id="browser" size="11">
						<datalist id="browsers">
							<?php
							$departments = [
								"None" => "None",
								"MAT" => "Khoa học máy tính",
								"KDL" => "Khoa học vật liệu"
							];
							foreach ($departments as $key => $val) {
								echo "<option value=$key>$val</option>";
							}
							?>
						</datalist>
						<div class="arrow"></div>
					</div>
				</div>
				<div class="register-container center">
					<button>Đăng ký</button>
				</div>
			</div>
		</div>
	</div>

</body>


</html>